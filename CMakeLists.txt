cmake_minimum_required(VERSION 3.8)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_VERBOSE_MAKEFILE OFF)

# Toolchain variables must be set before project() call.
set(AVR_UPLOADTOOL avrdude)
set(AVR_PROGRAMMER arduino)
set(AVR_UPLOADTOOL_PORT /dev/ttyACM0)
set(AVR_MCU atmega328p)
set(MCU_SPEED "16000000UL")
set(AVR_L_FUSE 0xff)
set(AVR_H_FUSE 0xde)

# Set root arduino SDK path
# Should contain lib/version.txt
set(ARDUINO_SDK_PATH /opt/arduino/arduino-1.8.5)

# Link with floating point printf support (larger binary size)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-u,vfprintf -lprintf_flt")

# Include toolchain
include(./cmake/toolchains/generic-gcc-avr.cmake)

project(AVRPractice)

# Default to release
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# Print status
message(STATUS "AVR_UPLOADTOOL: ${AVR_UPLOADTOOL}")
message(STATUS "AVR_PROGRAMMER: ${AVR_PROGRAMMER}")
message(STATUS "AVR_UPLOADTOOL_PORT: ${AVR_UPLOADTOOL_PORT}")
message(STATUS "AVR_UPLOADTOOL_OPTIONS: ${AVR_UPLOADTOOL_OPTIONS}")
message(STATUS "AVR_MCU: ${AVR_MCU}")
message(STATUS "MCU_SPEED: ${MCU_SPEED}")
message(STATUS "AVR_L_FUSE: ${AVR_L_FUSE}")
message(STATUS "AVR_H_FUSE: ${AVR_H_FUSE}")
message(STATUS "CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

# Find AVR root path
if(DEFINED ENV{AVR_FIND_ROOT_PATH})
  set(CMAKE_FIND_ROOT_PATH $ENV{AVR_FIND_ROOT_PATH})
else()
  if(EXISTS "/opt/local/avr")
    set(CMAKE_FIND_ROOT_PATH "/opt/local/avr")
  elseif(EXISTS "/usr/avr")
    set(CMAKE_FIND_ROOT_PATH "/usr/avr")
  elseif(EXISTS "/usr/lib/avr")
    set(CMAKE_FIND_ROOT_PATH "/usr/lib/avr")
  elseif(EXISTS "/usr/local/CrossPack-AVR")
    set(CMAKE_FIND_ROOT_PATH "/usr/local/CrossPack-AVR")
  else()
    message(FATAL_ERROR "Please set AVR_FIND_ROOT_PATH in your environment.")
  endif()
endif()
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Not added automatically, since CMAKE_SYSTEM_NAME is "generic"
set(CMAKE_SYSTEM_INCLUDE_PATH "${CMAKE_FIND_ROOT_PATH}/include")
set(CMAKE_SYSTEM_LIBRARY_PATH "${CMAKE_FIND_ROOT_PATH}/lib")

message(STATUS "CMAKE_FIND_ROOT_PATH: ${CMAKE_FIND_ROOT_PATH}")
message(STATUS "CMAKE_SYSTEM_INCLUDE_PATH: ${CMAKE_SYSTEM_INCLUDE_PATH}")
message(STATUS "CMAKE_SYSTEM_LIBRARY_PATH: ${CMAKE_SYSTEM_LIBRARY_PATH}")

# Compiler specific build type options
if(CMAKE_BUILD_TYPE MATCHES Release)
  set(CMAKE_C_FLAGS_RELEASE "-Os -DNDEBUG")
  set(CMAKE_CXX_FLAGS_RELEASE "-Os -DNDEBUG")
endif()

if(CMAKE_BUILD_TYPE MATCHES RelWithDebInfo)
  set(CMAKE_C_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-Os -save-temps -g -gdwarf-3 -gstrict-dwarf")
endif()

if(CMAKE_BUILD_TYPE MATCHES Debug)
  set(CMAKE_C_FLAGS_DEBUG "-Og -save-temps -g -gdwarf-3 -gstrict-dwarf")
  set(CMAKE_CXX_FLAGS_DEBUG "-Og -save-temps -g -gdwarf-3 -gstrict-dwarf")
endif()

# Global compiler options
add_definitions(
  -DF_CPU=${MCU_SPEED}
  -fpack-struct
  -fshort-enums
  -funsigned-char
  -funsigned-bitfields
  -ffunction-sections
  -c
  -Wall
  -Wextra
  -Wduplicated-cond
  -Wduplicated-branches
  -Wno-missing-braces
  -Wlogical-op
  -Wrestrict
  -Wnull-dereference
  -Wpointer-arith
  -Wold-style-cast
  -Wuseless-cast
  -Wcast-qual
  -Wdouble-promotion
  #        -Wshadow # too strict, GCC warns about shadowing of constructor params (unlike clang)
  -Wformat=2
  -Wsign-conversion
  -Wno-format-nonliteral
  -Werror
  -pedantic
  -pedantic-errors
  )

set(sources_exe
  src/app/main.cpp
  src/app/app.cpp
  src/app/app.hpp
  )

# Main executable
add_avr_executable(
  avr-practice
  ${sources_exe}
  )

# Link libraries to main executable
target_link_libraries(
  avr-practice-${AVR_MCU}.elf
  libserial-${AVR_MCU}
  libutil-${AVR_MCU}
  liblcd-${AVR_MCU}
  )

# Libraries

# libserial
set(sources_libserial
  src/libserial/src/serial.cpp
  src/libserial/include/serial/serial.hpp
  )

add_avr_library(
  libserial
  ${sources_libserial}
  )

target_include_directories(
  libserial-${AVR_MCU}
  PUBLIC
  src/libserial/include
  )

#libutil
set(sources_libutil
  src/libutil/src/timer.cpp
  src/libutil/include/util/timer.hpp
  src/libutil/src/power.cpp
  src/libutil/include/util/power.hpp
  src/libutil/src/dht11.cpp
  src/libutil/include/util/dht11.hpp
  src/libutil/include/util/interrupt_guard.hpp
  src/libutil/include/util/io.hpp
  src/libutil/src/twi.cpp
  src/libutil/include/util/twi.hpp
  src/libutil/src/rtc.cpp
  src/libutil/include/util/rtc.hpp
  )

add_avr_library(
  libutil
  ${sources_libutil}
  )

target_include_directories(
  libutil-${AVR_MCU}
  PUBLIC
  src/libutil/include
  )

#liblcd
set(sources_liblcd
  src/liblcd/src/hd44780.cpp
  src/liblcd/include/lcd/hd44780.hpp
  )

add_avr_library(
  liblcd
  ${sources_liblcd}
  )

target_include_directories(
  liblcd-${AVR_MCU}
  PUBLIC
  src/liblcd/include
  )

# Link libraries to main executable
target_link_libraries(
  liblcd-${AVR_MCU}
  libutil-${AVR_MCU}
  )

set(all_target_sources
  ${sources_exe}
  ${sources_libserial}
  ${sources_libutil}
  ${sources_liblcd}
  )

# Get relative and absolute source files
set(all_target_sources_absolute)
foreach(source ${all_target_sources})
  list(APPEND all_target_sources_absolute "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
endforeach()

# -----------------------------------------------------------------------------
# clang-format
# -----------------------------------------------------------------------------

find_program(
  CLANG_FORMAT_EXE
  NAMES clang-format
  DOC "Path to clang-format executable"
  )
if(CLANG_FORMAT_EXE)
  message(STATUS "Found clang-format: " ${CLANG_FORMAT_EXE})

  # Format whole project in-place
  add_custom_target(
    format
    COMMAND ${CLANG_FORMAT_EXE}
    -i
    -style=file
    ${all_target_sources_absolute}
    )

  if(NOT MSVC)
    # Return non-zero if formatting is needed
    add_custom_target(
      format-check
      COMMAND /bin/sh
      -c
      '! $$\(${CLANG_FORMAT_EXE} -style=file --output-replacements-xml ${all_target_sources_absolute} | grep -c \"<replacement \" >/dev/null\)'
      )
  endif()
else()
  message(STATUS "Found clang-format: FALSE")
endif()
