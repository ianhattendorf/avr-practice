#include "serial/serial.hpp"

#include <avr/io.h>
#include <avr/pgmspace.h>

void Serial::init(const unsigned long baud) {
  if constexpr (!enabled_) {
    return;
  }

  UCSR0B |= _BV(RXEN0) | _BV(TXEN0);   // Enable RX and TX
  UCSR0C |= _BV(UCSZ00) | _BV(UCSZ01); // 8-bit data

#if USE_2X
  UCSR0A |= _BV(U2X0);
#else
  UCSR0A &= ~(_BV(U2X0));
#endif

  const unsigned long ubrr_value = ((F_CPU) + 8UL * baud) / (16UL * baud) - 1UL;
  // Check for overflow
  if (ubrr_value >= (1 << 12)) {
    // Handle overflow
  }

  UBRR0H = static_cast<uint8_t>(ubrr_value >> 8);
  UBRR0L = static_cast<uint8_t>(ubrr_value & 0xff);
}

char Serial::receive_char_blocking() {
  if constexpr (!enabled_) {
    return 0;
  }

  loop_until_bit_is_set(UCSR0A, RXC0);
  return static_cast<char>(UDR0);
}

void Serial::send_char_blocking(const char char_to_send) {
  if constexpr (!enabled_) {
    return;
  }

  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = static_cast<uint8_t>(char_to_send);
  loop_until_bit_is_set(UCSR0A, TXC0);
  // Only cleared automatically if interrupts are enabled
  UCSR0A = _BV(TXC0);
}

void Serial::send_chars_blocking(const char *chars_to_send) {
  for (; *chars_to_send; ++chars_to_send) {
    Serial::send_char_blocking(*chars_to_send);
  }
}

void Serial::send_char_blocking_P(const char &pchar_to_send) {
  Serial::send_char_blocking(static_cast<char>(pgm_read_byte(&pchar_to_send)));
}

void Serial::send_chars_blocking_P(const char *pchars_to_send) {
  for (uint8_t char_to_send = pgm_read_byte(pchars_to_send); char_to_send != '\0';
       char_to_send = pgm_read_byte(++pchars_to_send)) {
    Serial::send_char_blocking(static_cast<char>(char_to_send));
  }
}

int Serial::fdev_read(FILE *) {
  return Serial::receive_char_blocking();
}

int Serial::fdev_write(char to_write, FILE *) {
  Serial::send_char_blocking(to_write);
  return 0;
}

bool Serial::receive_available() {
  if constexpr (!enabled_) {
    return false;
  }

  return bit_is_set(UCSR0A, RXC0) != 0;
}

bool Serial::send_available() {
  if constexpr (!enabled_) {
    return false;
  }

  return bit_is_set(UCSR0A, UDRE0) != 0;
}
