#pragma once

#include <stdio.h>

class Serial {
public:
  static void init(unsigned long baud);
  static char receive_char_blocking();
  static void send_char_blocking(char char_to_send);
  static void send_chars_blocking(const char *chars_to_send);
  static void send_char_blocking_P(const char &pchar_to_send);
  static void send_chars_blocking_P(const char *pchars_to_send);
  static int fdev_write(char to_write, FILE *);
  static int fdev_read(FILE *);
  static bool receive_available();
  static bool send_available();

private:
#ifdef NSERIAL
  static constexpr bool enabled_ = false;
#else
  static constexpr bool enabled_ = true;
#endif
};
