#pragma once

#include <avr/io.h>

class Timer1 {
public:
  static void init(uint16_t milli_interval = 1);
  static uint32_t millis();
  static void interrupt_called();

private:
  static volatile uint32_t timer_count_;
  static uint16_t milli_interval_;
};

class Timer2 {
public:
  static void set_prescale(uint8_t prescale = 1);
  static void init();
  static void start();
  static void stop();
  // Expensive division
  static uint16_t current_us();
  static uint8_t current_ticks();
  static uint8_t us_to_ticks(uint16_t us);
  static constexpr uint8_t us_to_ticks(uint8_t prescale, uint16_t us) {
    return us * cycles_per_us / prescales[prescale];
  }
  static uint8_t overflow_count();
  static bool overflowed();
  static constexpr uint8_t calc_prescale(uint16_t min_us) {
    const uint32_t cycles_to_min_us = cycles_per_us * min_us;
    uint8_t prescale = 0;
    for (; prescale < num_prescales && cycles_to_min_us / prescales[prescale] > UINT8_MAX;
         ++prescale)
      ;
    // assert(cycles_to_min_us / prescales[prescale] <= UINT8_MAX);
    return prescale;
  }

  static void isr_overflow();

private:
  static uint8_t prescale_;
  static uint8_t overflow_count_;

  static constexpr uint8_t cycles_per_us = F_CPU / 1000000ul;
  static constexpr uint8_t num_prescales = 8;
  // 1st prescale is technically 'No clock source'
  static constexpr uint16_t prescales[] = {1, 1, 8, 32, 64, 128, 256, 1024};
  static_assert(sizeof(prescales) / sizeof(*prescales) == num_prescales);
};
