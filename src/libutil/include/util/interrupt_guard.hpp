#pragma once

#include <avr/interrupt.h>
#include <avr/io.h>

enum class Atomic : uint8_t { ForceOn, Restore };

template <Atomic restore_state = Atomic::Restore> class InterruptGuard {
public:
  InterruptGuard() : sreg_(restore_state == Atomic::Restore ? SREG : 0) { cli(); }

  InterruptGuard(const InterruptGuard &other) = delete;
  InterruptGuard(InterruptGuard &&other) noexcept = delete;
  InterruptGuard &operator=(const InterruptGuard &other) = delete;
  InterruptGuard &operator=(InterruptGuard &&other) noexcept = delete;

  ~InterruptGuard() noexcept {
    if constexpr (restore_state == Atomic::ForceOn) {
      sei();
    } else {
      if ((sreg_ & _BV(SREG_I)) != 0) {
        sei();
      }
    }
  }

private:
  const uint8_t sreg_;
};
