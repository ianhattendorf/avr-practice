#pragma once

#include <stdint.h>

namespace power {

enum class adc : uint8_t { ignore, off };

enum class bod : uint8_t { ignore, off };

enum class wdt : uint8_t {
  wdt_15ms = 0,
  wdt_30ms,
  wdt_60ms,
  wdt_120ms,
  wdt_250ms,
  wdt_500ms,
  wdt_1s,
  wdt_2s,
  wdt_4s,
  wdt_8s,
  wdt_off
};

void adc_noise_reduction(wdt wdt = wdt::wdt_off, adc adc = adc::ignore);

void power_save(wdt wdt, adc adc = adc::off, bod bod = bod::ignore);

void power_down(wdt wdt, adc adc = adc::off, bod bod = bod::ignore);
} // namespace power
