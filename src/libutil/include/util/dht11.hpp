#pragma once

#include "util/io.hpp"
#include "util/timer.hpp"
#include <stdint.h>

//! Structure to store sensor readings
struct DhtData {
  //! Temperature in celsius
  uint8_t temperature;

  //! Humidity in percent, 0-100
  uint8_t humidity;
};

/*! \brief Class to interact with DHT11 sensor
 *
 * Example: Dht11<REG_TAG(B), PB1> dht;
 *
 * Power and Data pin must be on the same port.
 */
template <class REG, uint8_t DATA_PIN, uint8_t POWER_PIN = 255> class Dht11 {
public:
  /*! \brief Read data from the sensor
   *
   * Sensor should be on for ~1 second before first reading.
   * Should only be called every 1-2 seconds (check datasheet).
   * On failure, \p data is untouched.
   *
   * \param data Pointer to \ref DhtData
   * \return true for success, false for failure
   */
  bool read(DhtData *data) const;

  //! Turn the sensor on
  void power_on() const;

  //! Turn the sensor off
  void power_off() const;

private:
  /*! \brief Expect a pulse (high or low, see \p level), timing out after \ref pulse_timeout_ticks
   * cycles
   *
   * \param level The pulse level. 0 = low, 1 = high
   * \return The loop iteration count (not clock cycles)
   */
  uint16_t expect_pulse(uint8_t level) const;

  /*! \brief Calculate the number of clock cycles per microsecond.
   *
   * Be careful with overflow (4095us max with 16MHz clock).
   * Invalid with any F_CPU less than 1MHz (will return 0).
   *
   * \param us The number of microseconds
   * \return The number of clock cycles
   */
  static constexpr uint16_t us_to_ticks(uint16_t us) { return F_CPU / 1000000 * us; }

  //! Timeout for DHT pulses in ticks (clock cycles)
  static constexpr uint16_t pulse_timeout_loops = us_to_ticks(250) / 12; // ~12 cycle loop
};
