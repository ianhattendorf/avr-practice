#pragma once

#include <stdint.h>

/*! \brief TWI class. Polling based, single master
 *
 * Functions return 0 for success, or the value of the most recent TW_STATUS on error.
 */
class Twi {
public:
  enum class AckNack : uint8_t { Ack, Nack };

  static uint8_t begin_write(uint8_t address);
  static uint8_t begin_read(uint8_t address);
  static uint8_t end();
  static uint8_t send_byte(uint8_t value);
  static uint8_t receive_byte(AckNack ack_nack, uint8_t *success);
  static void set_bitrate(uint32_t bitrate);

private:
  static uint8_t start();
  static uint8_t stop();
};
