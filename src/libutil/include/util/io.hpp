#pragma once

#include <avr/io.h>
#include <stdint.h>

#define REG_TAG(reg) reg_tag_for_##reg
#define MAKE_REG_TAG(reg)                                                                          \
  struct reg_tag_for_##reg {                                                                       \
    static constexpr volatile uint8_t &ddr() { return DDR##reg; }                                  \
    static constexpr volatile uint8_t &port() { return PORT##reg; }                                \
    static constexpr volatile uint8_t &pin() { return PIN##reg; }                                  \
  }

MAKE_REG_TAG(B);
MAKE_REG_TAG(C);
MAKE_REG_TAG(D);
