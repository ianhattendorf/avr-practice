#pragma once

#include <stdint.h>

class DateTime {
public:
  DateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);
  constexpr uint16_t year() const { return year_; }
  constexpr uint8_t month() const { return month_; }
  constexpr uint8_t day() const { return day_; }
  constexpr uint8_t hour() const { return hour_; }
  constexpr uint8_t minute() const { return minute_; }
  constexpr uint8_t second() const { return second_; }

private:
  uint16_t year_;
  uint8_t month_;
  uint8_t day_;
  uint8_t hour_;
  uint8_t minute_;
  uint8_t second_;
};

class Rtc {
public:
  static bool begin();
  static DateTime now();
  static bool set_time(const DateTime &time);

private:
  static constexpr uint8_t ds3231_address = 0x68;
  //! Binary to binary-coded decimal
  static constexpr uint8_t bin_to_bcd(uint8_t bin) { return (bin / 10) << 4 | (bin % 10); }
  //! Binary-coded decimal to binary
  static constexpr uint8_t bcd_to_bin(uint8_t bcd) { return ((bcd >> 4) * 10) + (bcd & 0x0F); }
};
