#include "util/twi.hpp"
#include "util/io.hpp"
#include <avr/io.h>
#include <util/twi.h>

uint8_t Twi::begin_write(uint8_t address) {
  if (const uint8_t ret = start(); ret != 0) {
    return ret;
  }

  // Transmit SLA+W
  TWDR = (address << 1) & (~_BV(0));
  TWCR = _BV(TWINT) | _BV(TWEN);
  // Wait for SLA+W to transmit
  loop_until_bit_is_set(TWCR, TWINT);
  // Check that SLA+W was transmitted
  if (const uint8_t status = TW_STATUS; status != TW_MT_SLA_ACK) {
    return status;
  }

  return 0;
}

uint8_t Twi::begin_read(uint8_t address) {
  if (const uint8_t ret = start(); ret != 0) {
    return ret;
  }

  // Transmit SLA+R
  TWDR = (address << 1) | _BV(0);
  TWCR = _BV(TWINT) | _BV(TWEN);
  // Wait for SLA+R to transmit
  loop_until_bit_is_set(TWCR, TWINT);
  // Check that SLA+R was transmitted
  if (const uint8_t status = TW_STATUS; status != TW_MR_SLA_ACK) {
    return status;
  }

  return 0;
}

uint8_t Twi::end() {
  return stop();
}

uint8_t Twi::send_byte(uint8_t value) {
  // Transmit data
  TWDR = value;
  TWCR = _BV(TWINT) | _BV(TWEN);
  // Wait for data to transmit
  loop_until_bit_is_set(TWCR, TWINT);
  // Check that data was transmitted
  if (const uint8_t status = TW_STATUS; status != TW_MT_DATA_ACK) {
    return status;
  }

  return 0;
}

uint8_t Twi::receive_byte(AckNack ack_nack, uint8_t *success) {
  // Receive data
  if (ack_nack == AckNack::Ack) {
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA);
  } else {
    TWCR = _BV(TWINT) | _BV(TWEN);
  }
  // Wait for data to transmit
  loop_until_bit_is_set(TWCR, TWINT);
  // Check that data was transmitted
  if (const uint8_t status = TW_STATUS;
      status != (ack_nack == AckNack::Ack ? TW_MR_DATA_ACK : TW_MR_DATA_NACK)) {
    *success = status;
    return 0;
  }

  *success = 0;
  return TWDR;
}

void Twi::set_bitrate(uint32_t bitrate) {
  uint32_t twbr = (F_CPU - 17 * bitrate) / (2 * bitrate);
  if (twbr > UINT8_MAX) {
    // need prescale
    twbr = UINT8_MAX;
  }
  TWBR = twbr;
}

uint8_t Twi::start() {
  // Transmit start
  TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
  // Wait for TWINT to be set
  loop_until_bit_is_set(TWCR, TWINT);
  // Check that start was transmitted
  if (const uint8_t status = TW_STATUS; status != TW_START && status != TW_REP_START) {
    return status;
  }

  return 0;
}

uint8_t Twi::stop() {
  // Transmit stop
  TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);

  // Do we need to wait for TWINT, or TWSTO?
  loop_until_bit_is_set(TWCR, TWSTO);
  // loop_until_bit_is_set(TWCR, TWINT);

  return 0;
}
