#include "util/dht11.hpp"
#include "util/interrupt_guard.hpp"
#include "util/power.hpp"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <util/delay.h>

template <class REG, uint8_t DATA_PIN, uint8_t POWER_PIN>
bool Dht11<REG, DATA_PIN, POWER_PIN>::read(DhtData *data) const {
  // Initial high data line ~250ms
  REG::ddr() &= ~_BV(DATA_PIN);
  REG::port() |= _BV(DATA_PIN);
  power::power_down(power::wdt::wdt_250ms);

  // Start signal: low for ~18ms
  REG::ddr() |= _BV(DATA_PIN);   // output
  REG::port() &= ~_BV(DATA_PIN); // low
  _delay_ms(20);
  // power_down here is causing issues
  // power::power_down(power::wdt::wdt_30ms);

  uint8_t response[5];
  {
    // Timing critical
    const InterruptGuard guard{};

    // Start reading data: pullup and small delay
    REG::ddr() &= ~_BV(DATA_PIN); // input
    REG::port() |= _BV(DATA_PIN); // pull-up
    _delay_us(10);

    if (expect_pulse(1) == 0) {
      puts_P(PSTR("Failed 20-40us high\r"));
      return false;
    }

    // Expect low for 80us
    if (expect_pulse(0) == 0) {
      puts_P(PSTR("Failed low 80us\r"));
      return false;
    }

    // Expect high for 80us
    if (expect_pulse(1) == 0) {
      puts_P(PSTR("Failed high 80us\r"));
      return false;
    }

    for (int8_t i = 0; i < 5; ++i) {
      for (int8_t j = 7; j >= 0; --j) {
        // 50us initial pulse
        const uint16_t initial_pulse_ticks = expect_pulse(0);
        if (initial_pulse_ticks == 0) {
          printf_P(PSTR("Failed 50us initial pulse, i: %" PRIi8 ", j: %" PRIi8 "\r\n"), i, j);
          return false;
        }
        // Data pulse: 26-28us = 0, 70us = 1
        const uint16_t receive_bit_ticks = expect_pulse(1);
        if (receive_bit_ticks == 0) {
          printf_P(PSTR("Failed data pulse, i: %" PRIi8 ", j: %" PRIi8 ", ticks: %" PRIu8 "\r\n"),
                   i, j, receive_bit_ticks);
          if (i > 0) {
            printf_P(PSTR("d0: %" PRIu8 "\r\n"), response[0]);
          }
          if (i > 2) {
            printf_P(PSTR("d2: %" PRIu8 "\r\n"), response[2]);
          }
          return false;
        }
        if (receive_bit_ticks > initial_pulse_ticks) {
          response[i] |= _BV(j);
        } else {
          response[i] &= ~_BV(j);
        }
      }
    }
  }

  // Verify parity
  if (((response[0] + response[1] + response[2] + response[3]) & 0xff) != response[4]) {
    puts_P(PSTR("Failed parity\r"));
    return false;
  }

  // DHT11 doesn't support negative or decimal values, so we ignore them
  data->temperature = response[2];
  data->humidity = response[0];

  return true;
}

template <class REG, uint8_t DATA_PIN, uint8_t POWER_PIN>
void Dht11<REG, DATA_PIN, POWER_PIN>::power_on() const {
  if (POWER_PIN < 8) {
    // Set power output high
    REG::ddr() |= _BV(POWER_PIN);
    REG::port() |= _BV(POWER_PIN);
  }

  // Set data input high
  REG::ddr() &= ~_BV(DATA_PIN);
  REG::port() |= _BV(DATA_PIN);
}

template <class REG, uint8_t DATA_PIN, uint8_t POWER_PIN>
void Dht11<REG, DATA_PIN, POWER_PIN>::power_off() const {
  if (POWER_PIN > 7) {
    return;
  }

  // Set input with pull-up
  REG::ddr() &= ~_BV(POWER_PIN);
  REG::port() |= _BV(POWER_PIN);
}

template <class REG, uint8_t DATA_PIN, uint8_t POWER_PIN>
uint16_t Dht11<REG, DATA_PIN, POWER_PIN>::expect_pulse(uint8_t level) const {
  uint16_t current_iteration = 0;
  while (level == 1 ? bit_is_set(REG::pin(), DATA_PIN) : bit_is_clear(REG::pin(), DATA_PIN)) {
    // ~12 cycle busy loop
    if (++current_iteration == pulse_timeout_loops) {
      return 0; // fail
    }
  }
  return current_iteration;
}

template class Dht11<REG_TAG(B), PB1>;
