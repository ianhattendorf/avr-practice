#include "util/timer.hpp"
#include <avr/interrupt.h>
#include <avr/power.h>

volatile uint32_t Timer1::timer_count_ = 0;
uint16_t Timer1::milli_interval_ = 1;

// TODO: allow re-init?
// TODO: timer2 for lower-power sleep mode?
void Timer1::init(uint16_t milli_interval) {
  milli_interval_ = milli_interval;
  uint32_t cycles_per_milli_interval = F_CPU / 1000 * milli_interval_ - 1;
  uint16_t prescale = 1;

  while (cycles_per_milli_interval / prescale > UINT16_MAX) {
    if (prescale < 64) {
      prescale *= 8;
    } else {
      prescale *= 4;
    }
  }
  cycles_per_milli_interval /= prescale;
  uint16_t prescale_reg;
  switch (prescale) {
  case 1:
    prescale_reg = _BV(CS10);
    break;
  case 8:
    prescale_reg = _BV(CS11);
    break;
  case 64:
    prescale_reg = _BV(CS11) | _BV(CS10);
    break;
  case 256:
    prescale_reg = _BV(CS12);
    break;
  case 1024:
    prescale_reg = _BV(CS12) | _BV(CS10);
    break;
  default:
    // warn somehow?
    prescale_reg = _BV(CS12) | _BV(CS10);
    cycles_per_milli_interval = UINT16_MAX;
    break;
  }

  OCR1A = static_cast<uint16_t>(cycles_per_milli_interval); // timer compare value
  TCCR1B |= _BV(WGM12);                                     // CTC mode
  TIMSK1 |= _BV(OCIE1A);                                    // Interupt on compare match
  TCCR1B |= prescale_reg;
}

uint32_t Timer1::millis() {
  return timer_count_;
}

void Timer1::interrupt_called() {
  timer_count_ += milli_interval_;
}

uint8_t Timer2::prescale_ = 1;
uint8_t Timer2::overflow_count_ = 0;

void Timer2::set_prescale(uint8_t prescale) {
  prescale_ = prescale;
}

void Timer2::init() {
  power_timer2_enable();
  overflow_count_ = 0;

  TCCR2A = 0;
  TCCR2B = prescale_;
}

void Timer2::start() {
  TCNT2 = 0;
}

void Timer2::stop() {
  TCCR2B = 0;
}

uint16_t Timer2::current_us() {
  const uint8_t count = current_ticks();
  return static_cast<uint32_t>(count) * prescales[prescale_] / cycles_per_us;
}

uint8_t Timer2::current_ticks() {
  return TCNT2;
}

uint8_t Timer2::us_to_ticks(uint16_t us) {
  return us_to_ticks(prescale_, us);
}

uint8_t Timer2::overflow_count() {
  return overflow_count_;
}

bool Timer2::overflowed() {
  return overflow_count() != 0;
}

void Timer2::isr_overflow() {
  ++overflow_count_;
}

ISR(TIMER1_COMPA_vect) {
  Timer1::interrupt_called();
}

ISR(TIMER2_OVF_vect) {
  Timer2::isr_overflow();
}
