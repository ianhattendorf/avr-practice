#include "util/rtc.hpp"
#include "util/twi.hpp"
#include <avr/pgmspace.h>
#include <inttypes.h>
#include <stdio.h>

DateTime::DateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute,
                   uint8_t second)
    : year_(year), month_(month), day_(day), hour_(hour), minute_(minute), second_(second) {}

bool Rtc::begin() {
  // 400000
  Twi::set_bitrate(100000);

  return true;
}

DateTime Rtc::now() {
  if (const uint8_t ret = Twi::begin_write(ds3231_address); ret != 0) {
    printf_P(PSTR("Failed begin_write: %" PRIu8 "\r\n"), ret);
  }
  // Start at seconds register
  if (const uint8_t ret = Twi::send_byte(0x00); ret != 0) {
    printf_P(PSTR("Failed send_byte: %" PRIu8 "\r\n"), ret);
  }

  if (const uint8_t ret = Twi::begin_read(ds3231_address); ret != 0) {
    printf_P(PSTR("Failed begin_read: %" PRIu8 "\r\n"), ret);
  }
  uint8_t receive_success{};
  const uint8_t seconds = Twi::receive_byte(Twi::AckNack::Ack, &receive_success);
  if (receive_success != 0) {
    puts_P(PSTR("Failed seconds"));
  }
  const uint8_t minutes = Twi::receive_byte(Twi::AckNack::Ack, &receive_success);
  if (receive_success != 0) {
    puts_P(PSTR("Failed minutes"));
  }
  const uint8_t hours = Twi::receive_byte(Twi::AckNack::Nack, &receive_success);
  if (receive_success != 0) {
    puts_P(PSTR("Failed hours"));
  }
  if (const uint8_t ret = Twi::end(); ret != 0) {
    printf_P(PSTR("Failed end: %" PRIu8 "\r\n"), ret);
  }

  printf_P(PSTR("Time bytes: %" PRIu8 ":%" PRIu8 ":%" PRIu8 "\r\n"), hours, minutes, seconds);

  return DateTime{1234, 1, 2, bcd_to_bin(hours), bcd_to_bin(minutes), bcd_to_bin(seconds)};
}

bool Rtc::set_time(const DateTime &time) {
  if (const uint8_t ret = Twi::begin_write(ds3231_address); ret != 0) {
    printf_P(PSTR("Failed begin_write: %" PRIu8 "\r\n"), ret);
  }
  // Start at seconds register
  if (const uint8_t ret = Twi::send_byte(0x00); ret != 0) {
    printf_P(PSTR("Failed send_byte: %" PRIu8 "\r\n"), ret);
  }

  if (const uint8_t ret = Twi::send_byte(bin_to_bcd(time.second())); ret != 0) {
    printf_P(PSTR("Failed to send seconds: %" PRIu8 "\r\n"), ret);
  }
  if (const uint8_t ret = Twi::send_byte(bin_to_bcd(time.minute())); ret != 0) {
    printf_P(PSTR("Failed to send minutes: %" PRIu8 "\r\n"), ret);
  }
  if (const uint8_t ret = Twi::send_byte(bin_to_bcd(time.hour())); ret != 0) {
    printf_P(PSTR("Failed to send hours: %" PRIu8 "\r\n"), ret);
  }
  // if (const uint8_t ret = Twi::send_byte(bin_to_bcd(0)); ret != 0) {
  //   printf_P(PSTR("Failed to send days: %" PRIu8 "\r\n"), ret);
  // }
  // if (const uint8_t ret = Twi::send_byte(bin_to_bcd(0)); ret != 0) {
  //   printf_P(PSTR("Failed to send date: %" PRIu8 "\r\n"), ret);
  // }
  // if (const uint8_t ret = Twi::send_byte(bin_to_bcd(0)); ret != 0) {
  //   printf_P(PSTR("Failed to send month: %" PRIu8 "\r\n"), ret);
  // }
  // if (const uint8_t ret = Twi::send_byte(bin_to_bcd(0)); ret != 0) {
  //   printf_P(PSTR("Failed to send year: %" PRIu8 "\r\n"), ret);
  // }

  if (const uint8_t ret = Twi::end(); ret != 0) {
    printf_P(PSTR("Failed end: %" PRIu8 "\r\n"), ret);
  }

  return true;
}
