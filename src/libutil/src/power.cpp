#include "util/power.hpp"
#include "util/interrupt_guard.hpp"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/atomic.h>

static void sleep(uint8_t mode, power::wdt wdt, power::adc adc, power::bod bod) {
  uint8_t adcsra;
  if (adc == power::adc::off) {
    adcsra = ADCSRA;
    ADCSRA = 0;
  }

  if (wdt != power::wdt::wdt_off) {
    wdt_enable(static_cast<uint8_t>(wdt));
    WDTCSR |= _BV(WDIE);
  }

  {
    const InterruptGuard<Atomic::ForceOn> guard{};
    set_sleep_mode(mode);
    sleep_enable();
    if (bod == power::bod::off) {
      sleep_bod_disable();
    }
  }

  sleep_cpu();
  sleep_disable();

  if (adc == power::adc::off) {
    ADCSRA = adcsra;
  }
}

namespace power {

void adc_noise_reduction(wdt wdt, adc adc) {
  sleep(SLEEP_MODE_ADC, wdt, adc, bod::ignore);
}

void power_save(wdt wdt, adc adc, bod bod) {
  sleep(SLEEP_MODE_PWR_SAVE, wdt, adc, bod);
}

void power_down(wdt wdt, adc adc, bod bod) {
  sleep(SLEEP_MODE_PWR_DOWN, wdt, adc, bod);
}

} // namespace power

ISR(WDT_vect) {
  wdt_disable();
}
