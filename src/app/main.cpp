#include "app.hpp"

int main() {
  App app;
  app.setup();
  while (true) {
    app.loop();
  }
}
