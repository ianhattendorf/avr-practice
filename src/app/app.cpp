#include "app.hpp"
#include "serial/serial.hpp"
#include "util/dht11.hpp"
#include "util/power.hpp"
#include "util/rtc.hpp"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <util/delay.h>

hd44780_t *lcd_stream_hd44780 = nullptr;

/**
 * Workaround to treat hd47780 instance as file, using fdevopen.
 * Only works with one instance...
 * @param value Character to write to LCD.
 * @return Write success.
 */
int lcd_stream_put(char value, FILE *) {
  if (lcd_stream_hd44780 == nullptr) {
    return 1;
  }
  lcd_stream_hd44780->write(static_cast<uint8_t>(value));
  return 0;
}

App::~App() {
  if (lcd_stream_hd44780 == &hd44780) {
    lcd_stream_hd44780 = nullptr;
  }
}

void App::setup() {
  clock_prescale_set(clock_div_1);
  power_timer0_disable();

  Serial::init(9600);
  fdevopen(Serial::fdev_write, Serial::fdev_read);
  puts_P(PSTR("OK.\r"));

  // Turn on DHT sensor
  puts_P(PSTR("Turning dht on\r"));
  dht.power_on();

  // Set RW pin to low
  DDRB |= _BV(DDB2);
  PORTB &= ~_BV(PORTB2);

  hd44780.init();
  hd44780.cursor_blinking(false);
  if (lcd_stream_hd44780 == nullptr) {
    lcd_stream_hd44780 = &hd44780;
  }
  lcd_stream = fdevopen(lcd_stream_put, nullptr);

  fputs_P(PSTR("Initializing..."), lcd_stream);
  power::power_down(power::wdt::wdt_1s);

  hd44780.clear();
  fprintf_P(lcd_stream, PSTR("F_CPU: %lu"), F_CPU);
  power::power_down(power::wdt::wdt_1s);

  hd44780.clear();
  fputs_P(PSTR("Initialized."), lcd_stream);

  Rtc::begin();
  // Rtc::set_time(DateTime{1, 2, 3, 17, 33, 25});

  sei();

  DDRB |= _BV(DDB5);
}

static float celsius_to_fahrenheit(float celsius) {
  return celsius * 1.8f + 32;
}

void App::loop() {
  if (Serial::receive_available()) {
    const char recv = Serial::receive_char_blocking();
    Serial::send_char_blocking(recv);
  }

  PORTB ^= _BV(DDB5);
  power::power_save(power::wdt::wdt_15ms);
  PORTB ^= _BV(DDB5);

  DhtData dht_data{};
  puts_P(PSTR("Reading dht\r"));
  const bool dht_success = dht.read(&dht_data);
  printf_P(PSTR("Done reading dht, s: %s\r\n"), dht_success ? "true" : "false");

  hd44780.clear();
  if (dht_success) {
    fprintf_P(lcd_stream, PSTR("Temp: %.0f %cF"), celsius_to_fahrenheit(dht_data.temperature),
              0xDF);
    hd44780.set_cursor(1, 0);
    fprintf_P(lcd_stream, PSTR("Humidity: %" PRIu8 "%%"), dht_data.humidity);
  } else {
    fputs_P(PSTR("DHT failed."), lcd_stream);
  }
  puts_P(PSTR("Wrote results\r"));

  power::power_down(power::wdt::wdt_2s);
  const DateTime now = Rtc::now();
  hd44780.clear();
  fprintf_P(lcd_stream, PSTR("Time: %02" PRIu8 ":%02" PRIu8 ":%02" PRIu8), now.hour(), now.minute(),
            now.second());

  power::power_down(power::wdt::wdt_2s);
}
