#pragma once

#include <avr/io.h>
#include <lcd/hd44780.hpp>
#include <stdio.h>
#include <util/dht11.hpp>

using hd44780_t = Hd44780<REG_TAG(B), REG_TAG(D), DDB4, DDB3, 255, DDD5, DDD4, DDD3, DDD2, 2, 16>;

class App {
public:
  ~App();

  void setup();
  void loop();

private:
  static uint16_t read_adc(uint8_t adcx);
  static float read_temp();

  unsigned long previous_millis = 0;
  hd44780_t hd44780;
  Dht11<REG_TAG(B), PB1> dht;
  FILE *lcd_stream = nullptr;
};
