#pragma once

#include "util/io.hpp"
#include <avr/io.h>
#include <stdint.h>

enum RegisterSelect : uint8_t { Instruction, Data };

enum ReadWriteSelect : uint8_t { Write, Read };

enum EntryModeSetOptions : uint8_t {
  /**
   * S. 1 = Accompanies display shift
   */
  AccompanyDisplayShift = _BV(0),
  /**
   * I/D. 1 = Increment, 0 = Decrement
   */
  CursorMoveDirection = _BV(1)
};

enum DisplayOnOffControlOptions : uint8_t {
  /**
   * B.
   */
  CursorBlinking = _BV(0),
  /**
   * C.
   */
  Cursor = _BV(1),
  /**
   * D.
   */
  Display = _BV(2)
};

enum CursorDisplayShiftOptions : uint8_t {
  /**
   * R/L. 1 = right, 0 = left
   */
  ShiftDirection = _BV(2),
  /**
   * S/C. 1 = display shift
   */
  DisplayShiftOrCursorMove = _BV(3)
};

enum FunctionSetOptions : uint8_t {
  /**
   * F. 1 = 5x10 dots, 0 = 5x8 dots
   */
  CharacterFont = _BV(2),
  /**
   * N. 1 = 2 lines, 0 = 1 line
   */
  DisplayLinesNumber = _BV(3),
  /**
   * DL. 1 = 8 bits, 0 = 4 bits
   */
  InterfaceDataLength = _BV(4)
};

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
class Hd44780 {
public:
  void init();
  void command(uint8_t command);
  void write(uint8_t value);
  void clear();
  void return_home();
  void display(bool enable);
  void cursor(bool enable);
  void cursor_blinking(bool enable);
  void set_cursor(uint8_t col, uint8_t row);

private:
  // untested methods, need work
  void scroll_left();
  void scroll_right();
  void type_forward(bool enable);
  void autoscroll(bool enable);

  void send(uint8_t value, RegisterSelect mode);
  uint8_t read(bool read_busy);
  void write_nibble(uint8_t nibble);
  void toggle_e();
  void busy_wait();

  uint8_t entry_mode_flags = EntryModeSetOptions::CursorMoveDirection;
  uint8_t display_flags = DisplayOnOffControlOptions::Display | DisplayOnOffControlOptions::Cursor |
                          DisplayOnOffControlOptions::CursorBlinking;
  uint8_t cursor_shift_flags = 0;

  static constexpr uint8_t CLEAR_DISPLAY = _BV(0);
  static constexpr uint8_t RETURN_HOME = _BV(1);
  static constexpr uint8_t ENTRY_MODE_SET = _BV(2);
  static constexpr uint8_t DISPLAY_ON_OFF_CONTROL = _BV(3);
  static constexpr uint8_t CURSOR_OR_DISPLAY_SHIFT = _BV(4);
  static constexpr uint8_t FUNCTION_SET = _BV(5);
  static constexpr uint8_t SET_CGRAM_ADDRESS = _BV(6);
  static constexpr uint8_t SET_DDRAM_ADDRESS = _BV(7);

  static constexpr uint8_t CURSOR_OFFSETS[] = {0x00, 0x40};

  static constexpr uint8_t DELAY_INIT_MS = 20;
  static constexpr uint8_t DELAY_INSTR_LONG_MS = 5;
  static constexpr uint8_t DELAY_INSTR_SHORT_MS = 2;
  static constexpr uint8_t DELAY_ENABLE_US = 1;
};
