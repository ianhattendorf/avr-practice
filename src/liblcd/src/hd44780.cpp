#include "lcd/hd44780.hpp"
#include <avr/io.h>
#include <util/delay.h>

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::init() {
  // Cannot use busy flag during init
  // Set pins as outputs, clear EN and RS bits
  REG_DATA::ddr() |= _BV(DATA_PIN_4) | _BV(DATA_PIN_5) | _BV(DATA_PIN_6) | _BV(DATA_PIN_7);
  REG_CONTROL::ddr() |= _BV(RS) | _BV(EN);
  REG_CONTROL::port() &= ~(_BV(EN)) & ~(_BV(RS));

  // If read functionality enabled, set RW as output and clear
  if (RW < 8) {
    REG_CONTROL::ddr() |= _BV(RW);
    REG_CONTROL::port() &= ~(_BV(RW));
  }
  _delay_ms(DELAY_INIT_MS);

  // Init requires sending FUNCTION_SET 3 times.
  // We only need to send higher-order nibble in 4-bit mode.
  write_nibble(0x3);
  toggle_e();
  _delay_ms(DELAY_INSTR_LONG_MS);

  write_nibble(0x3);
  toggle_e();
  _delay_ms(DELAY_INSTR_SHORT_MS);

  write_nibble(0x3);
  toggle_e();
  _delay_ms(DELAY_INSTR_SHORT_MS);

  // We can now use the busy flag (or continue with delay if RW not attached).
  write_nibble(0x2); // FUNCTION_SET in 8-bit mode to 4-bit mode, send only single nibble
  toggle_e();
  _delay_ms(DELAY_INSTR_LONG_MS);

  // We are now in 4-bit mode
  // Set number of lines and character font (5x8)
  if (ROW_COUNT == 2) {
    command(FUNCTION_SET | FunctionSetOptions::DisplayLinesNumber);
  } else {
    command(FUNCTION_SET);
  }
  // Display off
  command(DISPLAY_ON_OFF_CONTROL);
  // Display clear
  command(CLEAR_DISPLAY);
  // Entry mode set (increment & not with display shift)
  command(ENTRY_MODE_SET | entry_mode_flags);
  // Turn on display and cursor
  command(DISPLAY_ON_OFF_CONTROL | display_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::command(const uint8_t command) {
  if (RW < 8) {
    busy_wait();
    send(command, RegisterSelect::Instruction);
    return;
  }
  send(command, RegisterSelect::Instruction);
  if (command == RETURN_HOME) {
    _delay_ms(DELAY_INSTR_LONG_MS);
  } else {
    _delay_ms(DELAY_INSTR_SHORT_MS);
  }
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::write(const uint8_t value) {
  if (RW < 8) {
    busy_wait();
    send(value, RegisterSelect::Data);
    return;
  }
  send(value, RegisterSelect::Data);
  _delay_ms(DELAY_INSTR_SHORT_MS);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::send(const uint8_t value, const RegisterSelect mode) {
  // Set data pins as output
  REG_DATA::ddr() |= _BV(DATA_PIN_4) | _BV(DATA_PIN_5) | _BV(DATA_PIN_6) | _BV(DATA_PIN_7);

  // Set either instruction or data mode
  if (mode == RegisterSelect::Data) {
    REG_CONTROL::port() |= _BV(RS);
  } else {
    REG_CONTROL::port() &= ~(_BV(RS));
  }
  if (RW < 8) {
    // RW = 0, write data.
    REG_CONTROL::port() &= ~(_BV(RW));
  }

  // Higher order first
  write_nibble(value >> 4);
  toggle_e();
  write_nibble(value);
  toggle_e();
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
uint8_t Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
                ROW_COUNT, COL_COUNT>::read(const bool read_busy) {
  // This method shouldn't be called without RW set. If it is, just return 0.
  if (RW > 7) {
    return 0;
  }
  // Set data pins as input
  REG_DATA::ddr() &= ~_BV(DATA_PIN_4) & ~_BV(DATA_PIN_5) & ~_BV(DATA_PIN_6) & ~_BV(DATA_PIN_7);
  if (read_busy) {
    // RS = 0, read busy flag + addresses
    REG_CONTROL::port() &= ~_BV(RS);
  } else {
    // RS = 1, read data
    REG_CONTROL::port() |= _BV(RS);
  }
  // RW = 1, read mode
  REG_CONTROL::port() |= _BV(RW);

  volatile uint8_t data = 0;
  // Read high nibble
  REG_CONTROL::port() |= _BV(EN);
  _delay_us(DELAY_ENABLE_US);
  if (REG_DATA::port() & _BV(DATA_PIN_4)) {
    data |= _BV(4);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_5)) {
    data |= _BV(5);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_6)) {
    data |= _BV(6);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_7)) {
    data |= _BV(7);
  }
  REG_CONTROL::port() &= ~_BV(EN);

  _delay_us(DELAY_ENABLE_US);

  // Read low nibble
  REG_CONTROL::port() |= _BV(EN);
  _delay_us(DELAY_ENABLE_US);
  if (REG_DATA::port() & _BV(DATA_PIN_4)) {
    data |= _BV(0);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_5)) {
    data |= _BV(1);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_6)) {
    data |= _BV(2);
  }
  if (REG_DATA::port() & _BV(DATA_PIN_7)) {
    data |= _BV(3);
  }
  REG_CONTROL::port() &= ~_BV(EN);

  _delay_us(DELAY_ENABLE_US);

  return data;
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::write_nibble(uint8_t nibble) {
  // Remember to toggle_e() if not calling send()/write()/command().
  // Could just require data pins to be in order, and take care of in one go.
  // Set data pins
  if (nibble & _BV(0)) {
    REG_DATA::port() |= _BV(DATA_PIN_4);
  } else {
    REG_DATA::port() &= ~(_BV(DATA_PIN_4));
  }
  if (nibble & _BV(1)) {
    REG_DATA::port() |= _BV(DATA_PIN_5);
  } else {
    REG_DATA::port() &= ~(_BV(DATA_PIN_5));
  }
  if (nibble & _BV(2)) {
    REG_DATA::port() |= _BV(DATA_PIN_6);
  } else {
    REG_DATA::port() &= ~(_BV(DATA_PIN_6));
  }
  if (nibble & _BV(3)) {
    REG_DATA::port() |= _BV(DATA_PIN_7);
  } else {
    REG_DATA::port() &= ~(_BV(DATA_PIN_7));
  }
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::toggle_e() {
  REG_CONTROL::port() |= _BV(EN);
  _delay_us(DELAY_ENABLE_US);
  REG_CONTROL::port() &= ~(_BV(EN));
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::clear() {
  command(CLEAR_DISPLAY);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::return_home() {
  command(RETURN_HOME);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::display(bool enable) {
  if (enable) {
    display_flags |= DisplayOnOffControlOptions::Display;
  } else {
    display_flags &= ~DisplayOnOffControlOptions::Display;
  }
  command(DISPLAY_ON_OFF_CONTROL | display_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::cursor(bool enable) {
  if (enable) {
    display_flags |= DisplayOnOffControlOptions::Cursor;
  } else {
    display_flags &= ~DisplayOnOffControlOptions::Cursor;
  }
  command(DISPLAY_ON_OFF_CONTROL | display_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::cursor_blinking(bool enable) {
  if (enable) {
    display_flags |= DisplayOnOffControlOptions::CursorBlinking;
  } else {
    display_flags &= ~DisplayOnOffControlOptions::CursorBlinking;
  }
  command(DISPLAY_ON_OFF_CONTROL | display_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::scroll_left() {
  cursor_shift_flags |= CursorDisplayShiftOptions::DisplayShiftOrCursorMove;
  cursor_shift_flags &= ~CursorDisplayShiftOptions::ShiftDirection;
  command(CURSOR_OR_DISPLAY_SHIFT | cursor_shift_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::scroll_right() {
  cursor_shift_flags |= CursorDisplayShiftOptions::DisplayShiftOrCursorMove |
                        CursorDisplayShiftOptions::ShiftDirection;
  command(CURSOR_OR_DISPLAY_SHIFT | cursor_shift_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::type_forward(bool enable) {
  if (enable) {
    entry_mode_flags |= EntryModeSetOptions::AccompanyDisplayShift;
  } else {
    entry_mode_flags &= ~EntryModeSetOptions::AccompanyDisplayShift;
  }
  command(ENTRY_MODE_SET | entry_mode_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::autoscroll(bool enable) {
  if (enable) {
    entry_mode_flags |= EntryModeSetOptions::CursorMoveDirection;
  } else {
    entry_mode_flags &= ~EntryModeSetOptions::CursorMoveDirection;
  }
  command(ENTRY_MODE_SET | entry_mode_flags);
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::set_cursor(uint8_t row, uint8_t col) {
  if (row > ROW_COUNT - 1) {
    row = ROW_COUNT - 1;
  }
  if (col > COL_COUNT - 1) {
    col = COL_COUNT - 1;
  }
  command(SET_DDRAM_ADDRESS | (col + CURSOR_OFFSETS[row]));
}

template <class REG_CONTROL, class REG_DATA, uint8_t RS, uint8_t EN, uint8_t RW, uint8_t DATA_PIN_4,
          uint8_t DATA_PIN_5, uint8_t DATA_PIN_6, uint8_t DATA_PIN_7, uint8_t ROW_COUNT,
          uint8_t COL_COUNT>
void Hd44780<REG_CONTROL, REG_DATA, RS, EN, RW, DATA_PIN_4, DATA_PIN_5, DATA_PIN_6, DATA_PIN_7,
             ROW_COUNT, COL_COUNT>::busy_wait() {
  // Read busy flag + address counter
  // Loop until busy flag is 0
  // Long timeout while testing, ~1s
  for (long retry = 10000; retry > 0; --retry) {
    if ((read(true) & _BV(7)) == 0) {
      break;
    }
  }
  // Address counter is updated 4us after busy flag is cleared
  // Do we need to wait for this if all we care about is the busy flag?
  _delay_us(5);
}

template class Hd44780<REG_TAG(B), REG_TAG(D), DDB4, DDB3, 255, DDD5, DDD4, DDD3, DDD2, 2, 16>;
